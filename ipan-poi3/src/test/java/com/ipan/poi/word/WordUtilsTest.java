package com.ipan.poi.word;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.apache.poi.hwpf.usermodel.Table;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.kits.io.ResourceUtil;

/**
 * Word文档工具类Test
 * 
 * @author iPan
 * @version 2013-9-16
 */
public class WordUtilsTest {
	
	private static File file2003 = null;
	private static File file2007 = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		URL url = ResourceUtil.asUrl(WordUtilsTest.class, "doctest.doc");
		file2003 = new File(url.getFile());
		url = ResourceUtil.asUrl(WordUtilsTest.class, "doctest.docx");
		file2007 = new File(url.getFile());
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void getTextTest() {
		System.out.println("-----------read 2003 text---------------");
		System.out.println(WordUtils.getText(file2003));
		System.out.println("-----------read 2007 text---------------");
		System.out.println(WordUtils.getText(file2007));
	}
	
	// 2003的单元格文本需要去掉空格
	@Test
	public void getTableTest() {
		System.out.println("-----------read 2003 table---------------");
		Table table = WordUtils.getHwpfTables(file2003).get(0);
		for (int i=0, rows=table.numRows(); i<rows; ++i) {
			TableRow row = table.getRow(i);
			for (int j=0, cells=row.numCells(); j<cells; ++j) {
				TableCell cell = row.getCell(j);
				System.out.print(WordUtils.trimAll(cell.text()) + "\t");
			}
			System.out.println();
		}
		System.out.println("-----------read 2007 table---------------");
		XWPFTable table2 = WordUtils.getXwpfTables(file2007).get(0);
		List<XWPFTableRow> rowList = table2.getRows();
		for (int i=0, rows=rowList.size(); i<rows; ++i) {
			XWPFTableRow row = rowList.get(i);
			List<XWPFTableCell> cellList = row.getTableCells();
			for (int j=0, cells=cellList.size(); j<cells; ++j) {
				XWPFTableCell cell = cellList.get(j);
				System.out.print(cell.getText() + "\t");
			}
			System.out.println();
		}
	}

	// 2003表格包含在段落内，2007表格不包含在段落内；
	@Test
	public void getParagraphTest() {
		System.out.println("-----------read 2003 paragraph---------------");
		List<String> textList = WordUtils.getHwpfParagraph(file2003);
		for (String s : textList) {
			System.out.println(s);
		}
		System.out.println("-----------read 2007 paragraph---------------");
		textList = WordUtils.getXwpfParagraph(file2007);
		for (String s : textList) {
			System.out.println(s);
		}
	}
}
