package com.ipan.poi.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TEST_Bean")
public class TestBean {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	@Column(length = 50)
	private String f1;
	@Column
	private int f2;
	@Column
	private long f3;
	@Column
	private float f4;
	@Column
	private double f5;
	@Column
	private BigDecimal f6;
	@Temporal(TemporalType.DATE)
	private Timestamp f7;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getF1() {
		return f1;
	}

	public void setF1(String f1) {
		this.f1 = f1;
	}

	public int getF2() {
		return f2;
	}

	public void setF2(int f2) {
		this.f2 = f2;
	}

	public long getF3() {
		return f3;
	}

	public void setF3(long f3) {
		this.f3 = f3;
	}

	public float getF4() {
		return f4;
	}

	public void setF4(float f4) {
		this.f4 = f4;
	}

	public double getF5() {
		return f5;
	}

	public void setF5(double f5) {
		this.f5 = f5;
	}

	public BigDecimal getF6() {
		return f6;
	}

	public void setF6(BigDecimal f6) {
		this.f6 = f6;
	}

	public Timestamp getF7() {
		return f7;
	}

	public void setF7(Timestamp f7) {
		this.f7 = f7;
	}

	@Override
	public String toString() {
		return "TestBean [id=" + id + ", f1=" + f1 + ", f2=" + f2 + ", f3=" + f3 + ", f4=" + f4 + ", f5=" + f5 + ", f6="
				+ f6 + ", f7=" + f7 + "]";
	}

}
