package com.ipan.poi.orm;

import org.junit.Assert;
import org.junit.Test;



/**
 * 实体类到数据库表的命名策略接口Test
 * 
 * @author iPan
 * @version 2013-9-21
 */
public class NamingStrategyTest {
	private static NamingStrategy naming = new DefaultNamingStrategy();

	@Test
	public void classToTableName() {
		String result = naming.classToTableName("aaBbCc");
		Assert.assertEquals(result, "aa_bb_cc");
		result = naming.classToTableName("abc");
		Assert.assertEquals(result, "abc");
		result = naming.classToTableName("abC");
		Assert.assertEquals(result, "abc");
		result = naming.classToTableName("aBc");
		Assert.assertEquals(result, "a_bc");
	}
	
	@Test
	public void propertyToColumnName() {
		String result = naming.classToTableName("aaBbCc");
		Assert.assertEquals(result, "aa_bb_cc");
		result = naming.classToTableName("abc");
		Assert.assertEquals(result, "abc");
		result = naming.classToTableName("abC");
		Assert.assertEquals(result, "abc");
		result = naming.classToTableName("aBc");
		Assert.assertEquals(result, "a_bc");
	}

}
