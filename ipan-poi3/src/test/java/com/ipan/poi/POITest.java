package com.ipan.poi;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ipan.kits.reflect.ReflectionUtil;
import com.ipan.poi.bean.TestBean;
import com.ipan.poi.excel.WorkbookFactory;

public class POITest {
	private static Logger logger = LoggerFactory.getLogger(POITest.class);
	
	// 读取测试
	public static void testRead(String filePath) throws Exception {
		Workbook workBook = null;
		InputStream fin = null;
		try {
			fin = new FileInputStream(filePath);
			workBook = WorkbookFactory.create(fin);
			logger.info("Workbook Class: {}", workBook.getClass().getName());
			int sheetNum = workBook.getNumberOfSheets();
			for (int index=0; index<sheetNum; ++index) { // sheet loop
				Sheet sheet = workBook.getSheetAt(index);
				int rows = getValidRowIndex(sheet);
				logger.info("sheet name: {}, sheet rows: {}, valid rows: {}", 
						new Object[]{sheet.getSheetName(), sheet.getLastRowNum(), rows});
				for (int rowIndex=0; rowIndex<=rows; ++rowIndex) { // row loop
					Row row = sheet.getRow(rowIndex);
					int cells = row.getLastCellNum();
					logger.info("=> rows: {}, cells: {}", rowIndex, cells);
					for (int cellIndex=0; cellIndex<=cells; ++cellIndex) { // cell loop
						Cell cell = row.getCell(cellIndex);
						CellType cellType = getCellType(cell);
						Object cellValue = getCellValue(cell);
						if (cellValue instanceof Date) {
							cellValue = ((Date)cellValue).toLocaleString();
						}
						logger.info("=> cell: {}, type:{}, value: {}", new Object[]{cellIndex, cellType, cellValue});
					}
				}
			}
		} finally {
			if (fin != null) {
				fin.close();
			}
		}
	}
	
	// 获得有效的行标（注：第一列必须不能为空）
	public static int getValidRowIndex(Sheet sheet) {
		int lastRow = sheet.getLastRowNum();
		int rowIndex = 0;
		for (rowIndex=lastRow; rowIndex>0; --rowIndex) {
			Cell cell = sheet.getRow(rowIndex).getCell(0);
			if (cell == null) {
				continue;
			}
			CellType cellType = cell.getCellType();
			if (CellType.BLANK != cellType) {
				break;
			}
		}
		return rowIndex;
	}
	
	// 获取单元格内容
	public static Object getCellValue(Cell cell) {
		Object result = null;
		if (cell == null) {
			return result;
		}
		
		CellType type = cell.getCellType();
		switch(type) {
		case NUMERIC:
			double value = cell.getNumericCellValue();
			if (org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)) {
				result = org.apache.poi.ss.usermodel.DateUtil.getJavaDate(value);
			} else {
				result = value;
			}
			break;
		case STRING:
			result = cell.getStringCellValue();
			break;
		case FORMULA:
			cell.setCellType(CellType.NUMERIC);
			result = cell.getNumericCellValue();
			break;
		case BLANK:
			break;
		case BOOLEAN:
			result = cell.getBooleanCellValue();
			break;
		case ERROR:
			break;
		}
		return result;
	}
	
	public static void setCellValue(Cell cell, Object value, CellType type) {
		switch(type) {
		case NUMERIC:
			cell.setCellType(CellType.NUMERIC);
			if (value instanceof Date) {
				cell.setCellValue((Date) value);
			} else {
				cell.setCellValue((Double) value);
			}
			break;
		case STRING:
			cell.setCellType(CellType.STRING);
			cell.setCellValue((String) value);
			break;
		case FORMULA:
			cell.setCellType(CellType.NUMERIC);
			cell.setCellValue((Double) value);
			break;
		case BLANK:
			cell.setCellType(CellType.BLANK);
			break;
		case BOOLEAN:
			cell.setCellType(CellType.BOOLEAN);
			cell.setCellValue((Boolean) value);
			break;
		case ERROR:
			cell.setCellType(CellType.ERROR);
			cell.setCellValue((String) value);
			break;
		}
	}
	
	// 获取单元格内容（填充null为空字符串）
	public static Object getConvertCellValue(Cell cell) {
		Object value = getCellValue(cell);
		return (value != null) ? value : "";
	}
	
	public static String getCellValueToString(Cell cell) {
		Object value = getCellValue(cell);
		return (value != null) ? value.toString() : "";
	}
	
	// 获取单元格类型（-1指不存在）
	public static CellType getCellType(Cell cell) {
		return (cell != null) ? cell.getCellType() : CellType._NONE;
	}
	
	public static Cell createCell(Object bean, String fieldName, String format, Row row, int index) {
		Class<?> fieldType = ReflectionUtil.getPropertyType(bean, fieldName);
		Object fieldValue = ReflectionUtil.getProperty(bean, fieldName);
		Cell cell = null;
		Workbook workbook = row.getSheet().getWorkbook();
		// 字符串
		if (CharSequence.class.isAssignableFrom(fieldType)) {
			cell = row.createCell(index, CellType.STRING);
			cell.setCellValue((String) fieldValue);
			// 数字
		} else if (Number.class.isAssignableFrom(fieldType) || Integer.TYPE == fieldType || Short.TYPE == fieldType 
				|| Byte.TYPE == fieldType || Long.TYPE == fieldType || Float.TYPE == fieldType || Double.TYPE == fieldType
				|| BigDecimal.class == fieldType) {
			cell = row.createCell(index, CellType.NUMERIC);
			double value = (fieldValue != null) ? Double.parseDouble(fieldValue.toString()) : null;
			cell.setCellValue(value);
			if (format != null && format.length() > 0) {
				CellStyle style = workbook.createCellStyle();
				style.setDataFormat(workbook.createDataFormat().getFormat(format));
				cell.setCellStyle(style);
			} else {
				CellStyle style = workbook.createCellStyle();
				style.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
				cell.setCellStyle(style);
			}
			// 布尔
		} else if (Boolean.class == fieldType || Boolean.TYPE == fieldType) {
			cell = row.createCell(index, CellType.BOOLEAN);
			cell.setCellValue((Boolean) fieldValue);
			// 日期时间
		} else if (Date.class == fieldType || Timestamp.class == fieldType) {
			cell = row.createCell(index, CellType.NUMERIC);
			cell.setCellValue((Date) fieldValue);
			if (format != null && format.length() > 0) {
				CellStyle style = workbook.createCellStyle();
				style.setDataFormat(workbook.createDataFormat().getFormat(format));
				cell.setCellStyle(style);
			} else {
				CellStyle style = workbook.createCellStyle();
				style.setDataFormat(workbook.createDataFormat().getFormat("yyyy-mm-dd hh:mm:ss"));
				cell.setCellStyle(style);
			}
			// 其他类型
		} else {
			cell = row.createCell(index, CellType.STRING);
			String value = (fieldValue != null) ? fieldValue.toString() : null;
			cell.setCellValue(value);
		}
		return cell;
	}
	
	// 写入测试1：从内存对象写入到excel文件；
	public static void testWrite1(String writeFile) throws Exception {
		// data
		TestBean bean = new TestBean();
		// file out
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(writeFile);
			// create workbook
			Workbook workbook = WorkbookFactory.createByFileExtension(writeFile);
			// create sheet
			Sheet wtSheet = workbook.createSheet("sheet1");
			// create title
			Row row = wtSheet.createRow(0);
			Cell cell = row.createCell(0, CellType.STRING);
			cell.setCellValue("F1");
			cell = row.createCell(1, CellType.STRING);
			cell.setCellValue("F2");
			cell = row.createCell(2, CellType.STRING);
			cell.setCellValue("F3");
			cell = row.createCell(3, CellType.STRING);
			cell.setCellValue("F4");
			cell = row.createCell(4, CellType.STRING);
			cell.setCellValue("F5(0.00)");
			cell = row.createCell(5, CellType.STRING);
			cell.setCellValue("F6(￥#,##0.00)");
			cell = row.createCell(6, CellType.STRING);
			cell.setCellValue("F7(yyyy-MM-dd hh:mm:ss)");
			// create body
			for (int i=1; i<6; ++i) {
				if (i == 2) {
					// 测试为null时候，写入文件是空字符串还是null字符串；
					// 结构是空字符串；
					bean.setF1(null);
				} else {
					bean.setF1("你好" + i);
				}
				bean.setF2(i);
				bean.setF3(100000000L + i);
				bean.setF4(100.555F + i);
				bean.setF5(100000000.444 + i);
				bean.setF6(new BigDecimal("1000.555").add(new BigDecimal(i)));
				bean.setF7(new Timestamp(Calendar.getInstance().getTimeInMillis()));
				row = wtSheet.createRow(i);
				// f1
				createCell(bean, "f1", null, row, 0);
				// f2
				createCell(bean, "f2", null, row, 1);
				// f3
				createCell(bean, "f3", null, row, 2);
				// f4
				createCell(bean, "f4", null, row, 3);
				// f5
				createCell(bean, "f5", "0.000", row, 4);
				// f6
				createCell(bean, "f6", "￥#,##0.00", row, 5);
				// f7
				// 经测试，发现MM与mm系统会智能解析是月还是分的；月的MM大小写无关；
				createCell(bean, "f7", "yyyy-mm-dd hh:mm:ss", row, 6);
				// 经测试，发现通过程序设置的DataFormat生成文档的数字格式都属于“自定义”；
			}
			workbook.write(fout);
		} finally {
			if (fout != null) {
				fout.close();
			}
		}
	}
	
	// 写入测试2：将readFile的第一个sheet，以行为单位，复制到writeFile的第一个sheet；
	public static void testWrite2(String readFile, String writeFile) throws Exception {
		Workbook rBook = null;
		Workbook wBook = null;
		FileInputStream fin = null;
		FileOutputStream fout = null;
		try {
			fin = new FileInputStream(readFile);
			fout = new FileOutputStream(writeFile);
			rBook = WorkbookFactory.create(fin);
			wBook = WorkbookFactory.createByFileExtension(writeFile);
			Sheet wSheet = wBook.createSheet("sheet1");
			Sheet rSheet = rBook.getSheetAt(0);
			
			// 复制需要合并的单元格
			// 注意：getNumMergedRegions从1开始；
			for (int index=0, mergedNum=rSheet.getNumMergedRegions(); index<mergedNum; ++index) {
				CellRangeAddress region = rSheet.getMergedRegion(index);
				wSheet.addMergedRegion(region);
			}
			
			// 设置列宽
			for (int cellIndex=0, cells=rSheet.getLastRowNum(); cellIndex<=cells; ++cellIndex) {
				wSheet.setColumnWidth(cellIndex, rSheet.getColumnWidth(cellIndex));
			}
			
			// 复制行
			int rows = getValidRowIndex(rSheet);
			for (int i=0; i<=rows; ++i) {
				Row rRow = rSheet.getRow(i);
				Row wRow = wSheet.createRow(i);
				copyRow(rRow, wRow);
			}
			wBook.write(fout);
		} finally {
			if (fin != null) {
				try {
					fin.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (fout != null) {
				try {
					fout.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void copyRow(Row fromRow, Row toRow) {
		// 设置行高
		toRow.setHeight(fromRow.getHeight());
		// 复制单元格
		int cellNum = fromRow.getLastCellNum();
		for (int i=0; i<cellNum; ++i) {
			Cell fromCell = fromRow.getCell(i);
			CellType fromType = getCellType(fromCell);
			fromType = (fromType != CellType._NONE) ? fromType : CellType.STRING;
			Object fromValue = getCellValue(fromCell);
			Cell toCell = CellUtil.getCell(toRow, i);
			setCellValue(toCell, fromValue, fromType);
			if (fromCell != null) {
				CellStyle fromStyle = fromCell.getCellStyle();
				// 注意：这种写法是错误的，必须按照下面重新new一个CellStyle；
				//toRow.getCell(i).getCellStyle().cloneStyleFrom(fromStyle);
				CellStyle toStyle = toRow.getSheet().getWorkbook().createCellStyle();
				toStyle.cloneStyleFrom(fromStyle);
				toRow.getCell(i).setCellStyle(toStyle);
			}
		}
	}
	
	public static void main(String[] args) throws Exception {
		// 读取文件路径
		String xlsFile = "f:/TestBean_test.xls";
		String xlsxFile = "f:/TestBean_test2.xlsx";
		// 写入文件路径
		String writeFile_beanToXls = "f:/beanToXls.xls";
		String writeFile_beanToXlsx = "f:/beanToXls.xlsx";
		String writeFile_xlsToXls = "f:/xlsToXls.xls";
		String writeFile_xlsxToXlsx = "f:/xlsxToXls.xlsx";
		
		// 读取测试
		// test read 2003
		logger.info("------------------------test read excel2003----------------------");
		testRead(xlsFile);
		// test read 2007
		logger.info("------------------------test read excel2007----------------------");
		testRead(xlsxFile);
		
		// 写入测试1
		testWrite1(writeFile_beanToXls);
		testWrite1(writeFile_beanToXlsx);
		
		// 写入测试2
		logger.info("------------------------test write excel 2003=>2003----------------------");
		testWrite2(xlsFile, writeFile_xlsToXls);
		logger.info("------------------------test write excel 2007=>2007----------------------");
		testWrite2(xlsxFile, writeFile_xlsxToXlsx);
	}
}
