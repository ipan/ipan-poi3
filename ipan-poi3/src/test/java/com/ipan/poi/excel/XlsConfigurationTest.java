package com.ipan.poi.excel;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.poi.excel.config.ExportConfiguration;
import com.ipan.poi.excel.config.ImportConfiguration;
import com.ipan.poi.excel.config.XlsRoot;

/**
 * 配置文件Test
 * 
 * @author iPan
 * @version 2013-9-14
 */
public class XlsConfigurationTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void importCfgTest() {
		XlsRoot root = ImportConfiguration.getInstance().getRoot();
		System.out.println(root);
	}

	@Test
	public void exportCfgTest() {
		XlsRoot root = ExportConfiguration.getInstance().getRoot();
		System.out.println(root);
	}
}
