package com.ipan.poi.excel.exporter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.kits.io.ResourceUtil;
import com.ipan.kits.time.DateFormatUtil;
import com.ipan.poi.PoiConfig;
import com.ipan.poi.bean.Employee;
import com.ipan.poi.excel.WorkbookFactory;
import com.ipan.poi.excel.config.ExportConfiguration;
import com.ipan.poi.excel.config.XlsEntity;
import com.ipan.poi.jdbc.JDBCConfig;
import com.ipan.poi.jdbc.JDBCManager;

/**
 * 写入操作Test
 * 
 * @author iPan
 * @version 2013-10-1
 */
public class XlsWritableTest {
	
	private static XlsWritable writer = null;
	private static XlsEntity defEntity = null;
	private static CellStyleCreatable style = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// 初始化配置文件
		URL cfgUrl = ResourceUtil.asUrl("xls_export_cfg.xml");
		File cfgFile = new File(cfgUrl.getFile());
		writer = new BaseXlsWriter();
		defEntity = ExportConfiguration.getInstance().addConfig(cfgFile).getEntity(Employee.class.getName());
		style = new DefaultCellStyleCreater(IndexedColors.CORNFLOWER_BLUE.index);
	}

	@Test
	public void testWriteByEntity() {
		Workbook workbook = null;
		Sheet sheet = null;
		FileOutputStream fout = null;
		List<Employee> entityList = new ArrayList<Employee>();
		Employee emp = null;
		for (int i=0; i<10; ++i) {
			Timestamp date = new Timestamp(new Date().getTime());
			emp = new Employee();
			emp.setNo((1000 + i) + "");
			emp.setName("name-" + i);
			emp.setAge(20 + i);
			try {
				emp.setBirthday(DateFormatUtil.parseDate("yyyy-MM-dd", (2013-29-i) + "-10-1"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			emp.setSalary(new BigDecimal("3000.55").add(new BigDecimal(i * 100)));
			emp.setCreateTime(date);
			emp.setUpdateTime(date);
			entityList.add(emp);
		}
		
		try {
			workbook = WorkbookFactory.createByFileExtension(".xls");
			sheet = workbook.createSheet("sheet1");
			fout = new FileOutputStream("c:/entity_test.xls");
			writer.writeByEntity(defEntity, sheet, style, 0, entityList);
			workbook.write(fout);
		} catch (IOException e) {
			throw new RuntimeException("Excel输出错误！", e);
		} finally {
			if (fout != null) {
				try {
					fout.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Test
	public void testWriteByMap() {
		Workbook workbook = null;
		Sheet sheet = null;
		FileOutputStream fout = null;
		List<Map<String, Object>> entityList = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = null;
		for (int i=0; i<10; ++i) {
			Timestamp date = new Timestamp(new Date().getTime());
			map = new HashMap<String, Object>();
			map.put("no", (1000 + i) + "");
			map.put("name", "name-" + i);
			map.put("age", 20 + i);
			try {
				map.put("birthday", DateFormatUtil.parseDate("yyyy-MM-dd", (2013-29-i) + "-10-1"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			map.put("salary", new BigDecimal("3000.55").add(new BigDecimal(i * 100)));
			map.put("createTime", date);
			map.put("updateTime", date);
			entityList.add(map);
		}
		
		try {
			workbook = WorkbookFactory.createByFileExtension(".xls");
			sheet = workbook.createSheet("sheet1");
			fout = new FileOutputStream("c:/map_test.xls");
			writer.writeByMap(defEntity, sheet, style, 0, entityList);
			workbook.write(fout);
		} catch (IOException e) {
			throw new RuntimeException("Excel输出错误！", e);
		} finally {
			if (fout != null) {
				try {
					fout.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Test
	public void testWriteByResultSet() {
		Workbook workbook = null;
		Sheet sheet = null;
		JDBCConfig jdbcConfig = PoiConfig.getInstance().getJDBCConfig();
		Connection conn = null;
		PreparedStatement statement = null;
		ResultSet resultset = null;
		FileOutputStream fout = null;
		String sql = "select * from tbl_employee";
		try {
			conn = JDBCManager.createConnection(jdbcConfig);
			statement = conn.prepareStatement(sql);
			resultset = JDBCManager.ExecuteQuery(statement, sql, null);
			workbook = WorkbookFactory.createByFileExtension(".xls");
			sheet = workbook.createSheet("sheet1");
			fout = new FileOutputStream("c:/resultSet_test.xls");
			writer.writeByResultSet(defEntity, sheet, style, 0, resultset);
			workbook.write(fout);
		} catch (Exception e) {
			JDBCManager.throwJDBCException(e);
		} finally {
			JDBCManager.closeResultSet(resultset);
			JDBCManager.closeStatement(statement);
			JDBCManager.closeConnection(conn);
		}
	}

}
