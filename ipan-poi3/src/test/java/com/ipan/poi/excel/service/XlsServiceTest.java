package com.ipan.poi.excel.service;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.kits.io.ResourceUtil;
import com.ipan.kits.time.DateFormatUtil;
import com.ipan.poi.bean.Employee;
import com.ipan.poi.excel.config.ExportConfiguration;
import com.ipan.poi.excel.config.ImportConfiguration;
import com.ipan.poi.excel.config.XlsEntity;

/**
 * 数据库操作接口Test；
 * 
 * @author iPan
 * @version 2013-09-20
 */
public class XlsServiceTest {
	private static XlsEntity importXlsEntity = null;
	private static XlsEntity exportXlsEntity = null;
	private static Employee importEntity = null;
	private static Employee exportEntity = null;
	private static XlsService service = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		URL importUrl = ResourceUtil.asUrl("xls_import_cfg.xml");
		URL exportUrl = ResourceUtil.asUrl("xls_export_cfg.xml");
		File importFile = new File(importUrl.getFile());
		File exportFile = new File(exportUrl.getFile());
		importXlsEntity = ImportConfiguration.getInstance().addConfig(importFile).getEntity(Employee.class.getName());
		exportXlsEntity = ExportConfiguration.getInstance().addConfig(exportFile).getEntity(Employee.class.getName());
		importEntity = new Employee();
		Timestamp curTime = new Timestamp(new Date().getTime()); 
		importEntity.setNo("1000");
		importEntity.setName("小王");
		importEntity.setAge(20);
		importEntity.setBirthday(DateFormatUtil.parseDate("yyyy-MM-dd", "1994-9-1"));
		importEntity.setSalary(new BigDecimal("3000.555"));
		importEntity.setCreateTime(curTime);
		importEntity.setUpdateTime(curTime);
		exportEntity = new Employee();
		exportEntity.setName("小王");
		service = new DefaultXlsService();
	}

	@Test
	public void testInsertXls() {
		service.insertXls(importXlsEntity, importEntity);
	}
	
	@Test
	public void testSelectXls() {
		List<Employee> list = (List<Employee>) service.selectXls(importXlsEntity, importEntity);
		Assert.assertEquals(list.size(), 1);
		Assert.assertEquals(list.get(0).getNo(), "1000");
		Assert.assertEquals(list.get(0).getName(), "小王");
		Assert.assertEquals(list.get(0).getAge(), 20);
		Assert.assertEquals(DateFormatUtil.formatDate("yyyy-MM-dd", list.get(0).getBirthday()), "1994-09-01");
		Assert.assertEquals(list.get(0).getSalary().toString(), "3000.56");
	}

	@Test
	public void testUpdateXls() {
		importEntity.setSalary(new BigDecimal("3500"));
		service.updateXls(importXlsEntity, importEntity);
	}

	@Test
	public void testSearchXls() {
		List<Employee> list = (List<Employee>) service.searchXls(exportXlsEntity, exportEntity);
		Assert.assertEquals(list.size(), 1);
		Assert.assertEquals(list.get(0).getSalary().toString(), "3500.00");
	}

}
