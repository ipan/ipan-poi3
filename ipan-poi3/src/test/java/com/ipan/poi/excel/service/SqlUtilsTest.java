package com.ipan.poi.excel.service;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.kits.io.ResourceUtil;
import com.ipan.kits.time.DateFormatUtil;
import com.ipan.poi.bean.Employee;
import com.ipan.poi.excel.config.ExportConfiguration;
import com.ipan.poi.excel.config.ImportConfiguration;
import com.ipan.poi.excel.config.XlsEntity;
import com.ipan.poi.excel.util.SqlUtils;

import org.junit.Assert;

/**
 * SQL语句生成工具Test
 * 
 * @author iPan
 * @version 2013-9-20
 */
public class SqlUtilsTest {
	
	private static XlsEntity importXlsEntity = null;
	private static XlsEntity exportXlsEntity = null;
	private static Employee importEntity = null;
	private static Employee exportEntity = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		URL importUrl = ResourceUtil.asUrl("xls_export_cfg.xml");
		URL exportUrl = ResourceUtil.asUrl("xls_export_cfg.xml");
		File importFile = new File(importUrl.getFile());
		File exportFile = new File(exportUrl.getFile());
		importXlsEntity = ImportConfiguration.getInstance().addConfig(importFile).getEntity(Employee.class.getName());
		exportXlsEntity = ExportConfiguration.getInstance().addConfig(exportFile).getEntity(Employee.class.getName());
		importEntity = new Employee();
		Timestamp curTime = new Timestamp(new Date().getTime()); 
		importEntity.setNo("1000");
		importEntity.setName("小王");
		importEntity.setAge(20);
		importEntity.setBirthday(DateFormatUtil.parseDate("yyyy-MM-dd", "1994-9-1"));
		importEntity.setSalary(new BigDecimal("3000.555"));
		importEntity.setCreateTime(curTime);
		importEntity.setUpdateTime(curTime);
		exportEntity = new Employee();
		exportEntity.setName("小王");
	}

	@Test
	public void createSelectSqlTest() {
		String selectSql = SqlUtils.createSelectSql(importXlsEntity, importEntity);
		Assert.assertEquals(selectSql, "select * from tbl_employee where no = ?");
		// 未设置验证字段的情况
		importXlsEntity.getProperty("no").setValid(false);
		selectSql = SqlUtils.createSelectSql(importXlsEntity, importEntity);
		Assert.assertEquals(selectSql, null);
		importXlsEntity.getProperty("no").setValid(true);
	}

	@Test
	public void createInsertSqlTest() {
		String insertSql = SqlUtils.createInsertSql(importXlsEntity, importEntity);
		Assert.assertEquals(insertSql, "insert into tbl_employee (no, name, age, birthday, salary) values(?, ?, ?, ?, ?)");
	}
	
	@Test
	public void createUpdateSqlTest() {
		String updateSql = SqlUtils.createUpdateSql(importXlsEntity, importEntity);
		Assert.assertEquals(updateSql, "update tbl_employee set name = ?, age = ?, birthday = ?, salary = ? where no = ?");
		// 未设置验证字段的情况
		importXlsEntity.getProperty("no").setValid(false);
		updateSql = SqlUtils.createUpdateSql(importXlsEntity, importEntity);
		Assert.assertEquals(updateSql, null);
		importXlsEntity.getProperty("no").setValid(true);
	}
	
	@Test
	public void createSearchSqlTest() {
		String searchSql = SqlUtils.createSearchSql(exportXlsEntity, exportEntity);
		Assert.assertEquals(searchSql, "select * from tbl_employee where name = ?");
		// 未传入值
		exportEntity.setName("");
		searchSql = SqlUtils.createSearchSql(exportXlsEntity, exportEntity);
		Assert.assertEquals(searchSql, "select * from tbl_employee");
		// 未传入值
		exportEntity.setName(null);
		searchSql = SqlUtils.createSearchSql(exportXlsEntity, exportEntity);
		Assert.assertEquals(searchSql, "select * from tbl_employee");
		exportEntity.setName("小王");
	}
	
	@Test
	public void createSelectParamsTest() {
		Object[] param = SqlUtils.createSelectParams(importXlsEntity, importEntity);
		Assert.assertEquals(param.length, 1);
		Assert.assertEquals(param[0].toString(), "1000");
		// 未设置验证字段的情况
		importXlsEntity.getProperty("no").setValid(false);
		param = SqlUtils.createSelectParams(importXlsEntity, importEntity);
		Assert.assertEquals(param, null);
		importXlsEntity.getProperty("no").setValid(true);
	}
	
	@Test
	public void createInsertParamsTest() {
		Object[] param = SqlUtils.createInsertParams(importXlsEntity, importEntity);
		Assert.assertEquals(param[0].toString(), "1000");
		Assert.assertEquals(param[1].toString(), "小王");
		Assert.assertEquals((Integer)param[2], new Integer(20));
//		Assert.assertEquals(param[3], PoiDateHelper.getDate("1994-9-1", "yyyy-MM-dd"));
		try {
			Assert.assertEquals(param[3], DateFormatUtil.parseDate("yyyy-MM-dd", "1994-9-1"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Assert.assertEquals(param[4], new BigDecimal("3000.555"));
	}
	
	@Test
	public void createUpdateParamsTest() {
		Object[] param = SqlUtils.createUpdateParams(importXlsEntity, importEntity);
		Assert.assertEquals(param[0].toString(), "小王");
		Assert.assertEquals((Integer)param[1], new Integer(20));
//		Assert.assertEquals(param[2], PoiDateHelper.getDate("1994-9-1", "yyyy-MM-dd"));
		try {
			Assert.assertEquals(param[2], DateFormatUtil.parseDate("yyyy-MM-dd", "1994-9-1"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Assert.assertEquals(param[3], new BigDecimal("3000.555"));
		Assert.assertEquals(param[4].toString(), "1000");
		// 未设置验证字段的情况
		importXlsEntity.getProperty("no").setValid(false);
		param = SqlUtils.createUpdateParams(importXlsEntity, importEntity);
		Assert.assertEquals(param, null);
		importXlsEntity.getProperty("no").setValid(true);
	}
	
	@Test
	public void createSearchParamsTest() {
		exportEntity.setNo("1001");
		exportEntity.setName("小明");
		Object[] param = SqlUtils.createSearchParams(exportXlsEntity, exportEntity);
		Assert.assertEquals(param[0], "1001");
		Assert.assertEquals(param[1], "小明");
		exportEntity.setNo(null);
		param = SqlUtils.createSearchParams(exportXlsEntity, exportEntity);
		Assert.assertEquals(param[0], "小明");
		exportEntity.setName(null);
		param = SqlUtils.createSearchParams(exportXlsEntity, exportEntity);
		Assert.assertEquals(param, null);
		exportEntity.setName("小王");
	}
}
