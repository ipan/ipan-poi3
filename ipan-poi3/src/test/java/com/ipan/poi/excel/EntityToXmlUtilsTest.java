package com.ipan.poi.excel;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.poi.bean.Employee;
import com.ipan.poi.excel.util.XlsGenerateCfgUtils;

/**
 * 从实体类生成XML配置文件工具类Test
 * 
 * @author iPan
 * @version 2013-9-15
 */
public class EntityToXmlUtilsTest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void pojoToImportXmlTest() {
		Class obj = Employee.class;
		System.out.println(XlsGenerateCfgUtils.pojoToImportXml(obj));
	}
	
	@Test
	public void pojoToExportXmlTest() {
		Class obj = Employee.class;
		System.out.println(XlsGenerateCfgUtils.pojoToExportXml(obj));
	}

	@Test
	public void ejb3ToImportXmlTest() {
		Class obj = Employee.class;
		System.out.println(XlsGenerateCfgUtils.jpaToImportXml(obj));
	}
	
	@Test
	public void ejb3ToExportXmlTest() {
		Class obj = Employee.class;
		System.out.println(XlsGenerateCfgUtils.jpaToExportXml(obj));
	}
}
