package com.ipan.poi.excel.importer;

import java.io.File;
import java.net.URL;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ipan.kits.io.ResourceUtil;
import com.ipan.poi.bean.Employee;
import com.ipan.poi.excel.XlsObjectFactory;
import com.ipan.poi.excel.config.ImportConfiguration;
import com.ipan.poi.excel.hander.XlsHander;

import org.junit.Assert;

/**
 * XLS导入功能定义Test
 * 
 * @author iPan
 * @version 2013-9-28
 */
public class XlsImporterTest {
	
	private static XlsImportable importer;
	private static File importFile;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// 初始化配置文件
		URL cfgUrl = ResourceUtil.asUrl("xls_import_cfg.xml");
		File cfgFile = new File(cfgUrl.getFile());
		ImportConfiguration.getInstance().addConfig(cfgFile);
		// 创建导入处理器
		importer = new XlsImporter();
		// 创建导入文件
		URL url = ResourceUtil.asUrl("employee.xls");
		importFile = new File(url.getFile());
	}

	@Test
	public void testImportExcelFile() {
		XlsHander hander = XlsObjectFactory.getDefaultXlsHander();
		XlsResult result = importer.importExcel(importFile, null, Employee.class.getName(), false, hander);
		System.out.println(result);
	}

	@Test
	public void testGetImportDir() {
		String dir = importer.getImportDir();
		Assert.assertEquals(dir, "c:/poi/excel");
	}

	@Test
	public void testGetImportSuccessDir() {
		String dir = importer.getImportSuccessDir();
		Assert.assertEquals(dir, "c:/poi/excel/success");
	}

	@Test
	public void testGetImportFailDir() {
		String dir = importer.getImportFailDir();
		Assert.assertEquals(dir, "c:/poi/excel/fail");
	}
}
