package com.ipan.poi.orm;

/**
 * 实体类到数据库表的命名策略接口
 * 
 * @author iPan
 * @version 2013-9-21
 */
public interface NamingStrategy {

	public String classToTableName(String className);
	
	public String propertyToColumnName(String propertyName);
}
