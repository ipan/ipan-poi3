package com.ipan.poi;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ipan.kits.io.ResourceUtil;
import com.ipan.kits.reflect.ClassLoaderUtil;
import com.ipan.poi.excel.config.XlsConfiguration;
import com.ipan.poi.jdbc.JDBCConfig;
import com.ipan.poi.orm.NamingStrategy;

/**
 * 系统配置文件
 * 
 * @author iPan
 * @version 2013-9-20
 */
public final class PoiConfig {
	
	private static PoiConfig INSTANCE = null;
	private static final String CONFIG_FILE = "ipan_cfg.properties";
	protected static Logger logger = LoggerFactory.getLogger(XlsConfiguration.class);

	/** excel导入/导出日志记录目录 */
	private String excelLogDir = "/ipan_poi/excel";
	private String driver = null;
	private String url = null;
	private String userName = null;
	private String passWord = null;
	private JDBCConfig jdbcCfg = null;
	private String namingStrategy = "com.ipan.poi.orm.DefaultNamingStrategy";
	private boolean xlsImporterValidate = true;
	private boolean xlsImporterLog = true;
	private boolean xlsExporterLog = true;

	private PoiConfig() {
		File file = null;
		URL url = null;
		try {
			url = ResourceUtil.asUrl(CONFIG_FILE);
		} catch (Exception e) {
//			logger.warn("Config配置文件" + CONFIG_FILE + "找不到！");
			return ;
		}
		if (url == null) {
//			logger.warn("Config配置文件" + CONFIG_FILE + "找不到！");
			return ;
		}
		file = new File(url.getFile());
		loadFile(file);
	}

	public void loadFile(File file) {
		InputStream fin = null;
		try {
			fin = new FileInputStream(file);
			Properties pro = new Properties();
			pro.load(fin);
			// 支持jdbc的配置
			driver = pro.getProperty("jdbc.driver");
			url = pro.getProperty("jdbc.url");
			userName = pro.getProperty("jdbc.username");
			passWord = pro.getProperty("jdbc.password");
			// 支持hibernate的配置
			loadHibernateCfg(pro);
			if (StringUtils.isNotBlank(pro.getProperty("ipan.poi.excel.log.dir"))) {
				excelLogDir = pro.getProperty("ipan.poi.excel.log.dir");
			}
			if (StringUtils.isNotBlank(pro.getProperty("ipan.poi.namingStrategy"))) {
				namingStrategy = pro.getProperty("ipan.poi.namingStrategy");
			}
			if (StringUtils.isNotBlank(pro.getProperty("ipan.poi.xlsImporter.validate"))) {
				String validate = pro.getProperty("ipan.poi.xlsImporter.validate");
				this.xlsImporterValidate = Boolean.parseBoolean(validate);
			}
			if (StringUtils.isNotBlank(pro.getProperty("ipan.poi.excel.log.xlsImporter.enable"))) {
				String enable = pro.getProperty("ipan.poi.excel.log.xlsImporter.enable");
				this.xlsImporterLog = Boolean.parseBoolean(enable);
			}
			if (StringUtils.isNotBlank(pro.getProperty("ipan.poi.excel.log.xlsExporter.enable"))) {
				String enable = pro.getProperty("ipan.poi.excel.log.xlsExporter.enable");
				this.xlsExporterLog = Boolean.parseBoolean(enable);
			}
		} catch (IOException e) {
			throw new RuntimeException("Config配置文件加载失败！", e);
		} finally {
			if (fin != null) {
				try {
					fin.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static synchronized PoiConfig getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PoiConfig();
		}
		return INSTANCE;
	}

	public synchronized JDBCConfig getJDBCConfig() {
		if (jdbcCfg == null) {
			jdbcCfg = new JDBCConfig(this.driver, this.url, this.userName, this.passWord);
		}
		return jdbcCfg;
	}
	
	public boolean isXlsImporterValidate() {
		return xlsImporterValidate;
	}

	public void setXlsImporterValidate(boolean xlsImporterValidate) {
		this.xlsImporterValidate = xlsImporterValidate;
	}
	
	public boolean isXlsImporterLog() {
		return xlsImporterLog;
	}

	public void setXlsImporterLog(boolean xlsImporterLog) {
		this.xlsImporterLog = xlsImporterLog;
	}

	public boolean isXlsExporterLog() {
		return xlsExporterLog;
	}

	public void setXlsExporterLog(boolean xlsExporterLog) {
		this.xlsExporterLog = xlsExporterLog;
	}

	public String getExcelLogDir() {
		return excelLogDir;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public void setExcelLogDir(String excelLogDir) {
		this.excelLogDir = excelLogDir;
	}

	public String getNamingStrategy() {
		return namingStrategy;
	}

	public void setNamingStrategy(String namingStrategy) {
		this.namingStrategy = namingStrategy;
	}

	public NamingStrategy createNamingStrategy() {
		NamingStrategy result = null;
		try {
			if (StringUtils.isNotBlank(this.namingStrategy)) {
				Class<?> strategy = ClassLoaderUtil.loadClass(this.namingStrategy, this.getClass());
				if (strategy != null) {
					result = (NamingStrategy) strategy.newInstance();
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("NamingStrategy创建失败！", e);
		}
		return result;
	}

	// 支持使用hibernate的配置连接（注意：两个同时配置的时候，以JDBC的配置为主；）
	private void loadHibernateCfg(Properties pro) {
		if (StringUtils.isBlank(this.driver)) {
			String driverClass = pro.getProperty("hibernate.connection.driver_class");
			if (driverClass != null) {
				this.driver = driverClass;
			}
		}
		if (StringUtils.isBlank(this.url)) {
			String urlStr = pro.getProperty("hibernate.connection.url");
			if (urlStr != null) {
				this.url = urlStr;
			}
		}
		if (StringUtils.isBlank(this.userName)) {
			String userStr = pro.getProperty("hibernate.connection.username");
			if (userStr != null) {
				this.userName = userStr;
			}
		}
		if (StringUtils.isBlank(this.passWord)) {
			String passStr = pro.getProperty("hibernate.connection.password");
			if (passStr != null) {
				this.passWord = passStr;
			}
		}
	}

}
