package com.ipan.poi.excel.exporter;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;

import com.ipan.poi.excel.config.XlsEntity;

/**
 * 写入操作默认实现；
 * 当出现样式改变的情况，只需要自定义实现CellStyleCreatable接口即可；
 * 当出现写入标题或写入正文的方式改变的情况，也就是不仅是改变样式了，那么，可以继承该类或者组合也行，自行实现XlsWritable接口；
 * 
 * @author iPan
 * @version 2013-09-20
 */
public class BaseXlsWriter implements XlsWritable {
	
	public int writeByEntity(XlsEntity defEntity, Sheet sheet, CellStyleCreatable style, int rowIndex, List<?> entityList) {
		int index = doWriteTitle(defEntity, sheet, style, rowIndex);
		index = doWriteBodyByEntity(defEntity, sheet, entityList, style, index);
		return index;
	}

	public int writeByMap(XlsEntity defEntity, Sheet sheet, CellStyleCreatable style, int rowIndex, List<Map<String, Object>> list) {
		int index = doWriteTitle(defEntity, sheet, style, rowIndex);
		index = doWriteBodyByMap(defEntity, sheet, list, style, index);
		return index;
	}

	public int writeByResultSet(XlsEntity defEntity, Sheet sheet, CellStyleCreatable style, int rowIndex, ResultSet resultSet) {
		int index = doWriteTitle(defEntity, sheet, style, rowIndex);
		index = doWriteBodyByResultSet(defEntity, sheet, resultSet, style, index);
		return index;
	}

	// 子类可以覆盖该方法
	protected int doWriteTitle(XlsEntity defEntity, Sheet sheet, CellStyleCreatable style, int rowIndex) {
		return WriterUtils.appendTitle(defEntity, sheet, style, rowIndex);
	}

	// 子类可以覆盖该方法
	protected int doWriteBodyByEntity(XlsEntity defEntity, Sheet sheet, List<?> entityList, CellStyleCreatable style, int rowIndex) {
		return WriterUtils.appendBodyByEntity(defEntity, entityList, sheet, style, rowIndex);
	}

	// 子类可以覆盖该方法
	protected int doWriteBodyByMap(XlsEntity defEntity, Sheet sheet, List<Map<String, Object>> list, CellStyleCreatable style, int rowIndex) {
		return WriterUtils.appendBodyByMap(defEntity, list, sheet, style, rowIndex);
	}

	// 子类可以覆盖该方法
	protected int doWriteBodyByResultSet(XlsEntity defEntity, Sheet sheet, ResultSet resultSet, CellStyleCreatable style, int rowIndex) {
		return WriterUtils.appendBodyByResultSet(defEntity, resultSet, sheet, style, rowIndex);
	}
	
}
