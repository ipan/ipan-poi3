package com.ipan.poi.excel.config;


/**
 * 导入配置；
 * 
 * @author iPan
 * @version 2013-09-14
 */
public class XlsImports extends XlsRoot {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4096456345279265346L;

	public static final String ELE_ROOT = "xlsImports";
	
	public XlsImports() {
		super(ELE_ROOT);
	}
	
}
