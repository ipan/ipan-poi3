package com.ipan.poi.excel.log;

/**
 * 导出文件记录工具类，支持多线程并发；
 * 
 * @author iPan
 * @version 2013-09-15
 */
public final class XlsExportFileLogger extends PoiAppFileLogger {
	
	// 导出MSG
	public static final String MSG_EXPORT_START = "导出开始：{0}";
	public static final String MSG_EXPORT_ENTITY = "导出实体：{0}";
	public static final String MSG_EXPORT_RESULT = "导出结果：{0}";
	public static final String MSG_EXPORT_FINISH = "导出结束：{0}";
	
	public XlsExportFileLogger(String filePath) {
		super(filePath);
	}

}
