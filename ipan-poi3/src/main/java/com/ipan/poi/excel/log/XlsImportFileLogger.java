package com.ipan.poi.excel.log;

/**
 * 导入文件记录工具类，支持多线程并发；
 * 
 * @author iPan
 * @version 2013-09-15
 */
public final class XlsImportFileLogger extends PoiAppFileLogger {
	
	// 导入MSG
	public static final String MSG_IMPORT_START = "导入开始：{0}";
	public static final String MSG_FILE = "导入文件：{0}";
	public static final String MSG_IMPORT_RESULT = "导入结果：{0}    总计：{1}条    更新：{2}条    插入：{3}条    失败：{4}条";
	public static final String MSG_BACKUP = "导入文件保存路径：{0}";
	public static final String MSG_FAIL_FILE = "失败记录文件保存路径：{0}";
	public static final String MSG_IMPORT_FINISH = "导入结束：{0}";
	public static final String MSG_PARSER_FAIL_ROW_NO = "解析失败：[{0}]Sheet ({1}行, {2}列)";
	public static final String MSG_SAVE_FAIL_ROW_NO = "保存失败：[{0}]Sheet {1}行";
	
	public XlsImportFileLogger(String filePath) {
		super(filePath);
	}

}
