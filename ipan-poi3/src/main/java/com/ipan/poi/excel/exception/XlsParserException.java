package com.ipan.poi.excel.exception;

/**
 * XLS解析异常类；
 * 
 * @author iPan
 * @version 2013-06-01
 */
public class XlsParserException extends Exception {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3184510690792308452L;
	
	private int errorCode = -1;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public XlsParserException() {
		super();
	}

	public XlsParserException(String message, Throwable cause) {
		super(message, cause);
	}

	public XlsParserException(String message) {
		super(message);
	}

	public XlsParserException(Throwable cause) {
		super(cause);
	}
	
}
