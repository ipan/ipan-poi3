package com.ipan.poi.excel.config;


/**
 * 导出配置；
 * 
 * @author iPan
 * @version 2013-09-14
 */
public class XlsExports extends XlsRoot {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2101139652370660446L;

	public static final String ELE_ROOT = "xlsExports";
	
	public XlsExports() {
		super(ELE_ROOT);
	}
}
