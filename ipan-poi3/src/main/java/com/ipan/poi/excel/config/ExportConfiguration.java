package com.ipan.poi.excel.config;


/**
 * 导出配置文件信息
 * 
 * @author iPan
 * @version 2013-9-14
 */
public class ExportConfiguration extends XlsConfiguration {
	
	public final static String DEFAULT_PATH = "xls_export_cfg.xml";
	
	private static ExportConfiguration INSTANCE = null;
	
	private String filePath = DEFAULT_PATH;
	
	private ExportConfiguration() {
		super(new XlsExports());
		flushConfig();
	}
	
	private ExportConfiguration(String filePath) {
		super(new XlsExports());
		this.filePath = filePath;
		flushConfig();
	}
	
	@Override
	public void flushConfig() {
		initDefaultConfig(this.filePath);
	}
	
	public synchronized static ExportConfiguration getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ExportConfiguration();
		}
		return INSTANCE;
	}
	
	public synchronized static ExportConfiguration getInstance(String filePath) {
		if (INSTANCE == null) {
			INSTANCE = new ExportConfiguration(filePath);
		}
		return INSTANCE;
	}
}
