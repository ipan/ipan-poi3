package com.ipan.poi.excel;

import com.ipan.poi.excel.exporter.CellStyleCreatable;
import com.ipan.poi.excel.exporter.DefaultCellStyleCreater;
import com.ipan.poi.excel.exporter.BaseXlsWriter;
import com.ipan.poi.excel.exporter.XlsExportable;
import com.ipan.poi.excel.exporter.XlsExporter;
import com.ipan.poi.excel.exporter.XlsWritable;
import com.ipan.poi.excel.hander.BaseXlsHander;
import com.ipan.poi.excel.hander.XlsHander;
import com.ipan.poi.excel.importer.XlsImporter;
import com.ipan.poi.excel.service.DefaultXlsService;
import com.ipan.poi.excel.service.XlsService;

/**
 * Excel对象工厂
 * 
 * @author iPan
 * @version 2015-01-10
 */
public final class XlsObjectFactory {
	private static XlsService<Object> DEFAULT_SERVICE = new DefaultXlsService();
	private static XlsHander DEFAULT_HANDER = new BaseXlsHander(DEFAULT_SERVICE);
	private static XlsWritable DEFAULT_WRITER = new BaseXlsWriter();

	/**
	 * 创建导入接口
	 */
	public static XlsImporter createXlsImporter() {
		return new XlsImporter();
	}
	
	/**
	 * 创建导入接口
	 * 2016-04-13 新增
	 */
	public static XlsImporter createXlsImporter(CellStyleCreatable cellStyleCreater) {
		return new XlsImporter(cellStyleCreater);
	}
	
	/**
	 * 创建导出接口
	 */
	public static XlsExportable createXlsExporter() {
		return new XlsExporter();
	}
	
	/**
	 * 创建导出接口
	 */
	public static XlsExportable createXlsExporter(XlsWritable writer) {
		return new XlsExporter(writer);
	}
	
	/**
	 * 获取默认Excel解析器
	 */
	public static XlsHander getDefaultXlsHander() {
		return DEFAULT_HANDER;
	}
	
	/**
	 * 创建默认Excel解析器
	 */
	public static XlsHander createDefaultXlsHander(XlsService<Object> service) {
		return new BaseXlsHander(service);
	}
	
	/**
	 * 获取默认写入器
	 */
	public static XlsWritable getDefaultWriter() {
		return DEFAULT_WRITER;
	}
	
	/**
	 * 获取默认Service
	 */
	public static XlsService<Object> getDefaultXlsService() {
		return DEFAULT_SERVICE;
	}
	
	/**
	 * 创建CellStyleCreatable实例
	 */
	public static CellStyleCreatable createCellStyleCreater() {
		return new DefaultCellStyleCreater();
	}
	
	/**
	 * 创建CellStyleCreatable实例
	 * @param titleBackgroundColor 标题颜色
	 * @see org.apache.poi.ss.usermodel.IndexedColors
	 */
	public static CellStyleCreatable createCellStyleCreater(short titleBackgroundColor) {
		return new DefaultCellStyleCreater(titleBackgroundColor);
	}
	
}
