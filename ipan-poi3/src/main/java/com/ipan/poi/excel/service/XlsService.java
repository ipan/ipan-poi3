package com.ipan.poi.excel.service;

import java.util.List;

import com.ipan.poi.excel.config.XlsEntity;



/**
 * 数据库操作接口；
 * 
 * @author iPan
 * @version 2013-09-20
 */
public interface XlsService<T> {
	
	/**
	 * 返回验证的记录条数（导入）
	 * 根据配置文件的配置，查询对应实体的条数；
	 */
	public List<T> selectXls(XlsEntity defEntity, T entity);
	
	/**
	 * 插入实体到数据库（导入）
	 * 根据配置文件配置的字段，插入到数据库；
	 * 未配置的字段如何处理，需要根据各自项目的情况，自行实现该接口；默认JDBC实现是不会处理的；
	 */
	public void insertXls(XlsEntity defEntity, T entity);
	
	/**
	 * 更新实体到数据库（导入）
	 * 根据配置文件的配置，更新对应实体；
	 */
	public void updateXls(XlsEntity defEntity, T entity);
	
	/**
	 * 查询实体列表（导出）
	 * 根据配置文件配置的查询字段，查询该实体；
	 * 默认JDBC实现，查询配置的待查询字段的并集，如果字段为空就排除该查询字段；查询过滤取决于配合的待查询字段以及实体类的值；
	 * @see com.ipan.poi.excel.service.DefaultXlsService 参考默认实现；
	 */
	public List<T> searchXls(XlsEntity defEntity, T entity);
}
