package com.ipan.poi.excel.exporter;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * 默认格式化实现；
 * 
 * @author iPan
 * @version 2015-01-10
 */
public class DefaultCellStyleCreater implements CellStyleCreatable {
	/** 标题字体 */
	protected static final String DEFAULT_TITLE_FONTNAME = "宋体";
	/** 标题字号 */
	protected static final short DEFAULT_TITLE_FONTSIZE = 12;
	/** 正文字体 */
	protected static final String DEFAULT_BODY_FONTNAME = "宋体";
	/** 正文字号 */
	protected static final short DEFAULT_BODY_FONTSIZE = 10;

	/** 标题背景色 */
	protected Short titleBackgroundColor = null;
	/** 正文背景色 */
	protected Short bodyBackgroundColor = null;
	/** 标题字体 */
	protected Font titleFont;
	/** 正文字体 */
	protected Font bodyFont;
	/** 标题样式 */
	protected CellStyle titleStyle; // 2015-01-10修正：字体、样式的使用有限制，需要改成重用；
	/** 正文样式 */
//	protected CellStyle bodyStyle; // // 2015-01-10修正
	private Map<String, CellStyle> bodyStyleContext = new HashMap<String, CellStyle>(); // 单元格样式不能创建过多，但是共用一个又存在问题（日期、金额可能有自定义格式），所以只能根据自定义格式归类；2018-05-07
	
	public DefaultCellStyleCreater() {
	}

	/**
	 * 构造时候可指定颜色
	 * 
	 * @param titleBackgroundColor 标题颜色
	 * @see org.apache.poi.ss.usermodel.IndexedColors
	 */
	public DefaultCellStyleCreater(short titleBackgroundColor) {
		this.titleBackgroundColor = titleBackgroundColor;
	}
	
	public DefaultCellStyleCreater(short titleBackgroundColor, short bodyBackgroundColor, Font titleFont, Font bodyFont) {
		this.titleBackgroundColor = titleBackgroundColor;
		this.bodyBackgroundColor = bodyBackgroundColor;
		this.titleFont = titleFont;
		this.bodyFont = bodyFont;
	}

	public CellStyle createTitleStyle(Workbook workbook) {
		if (titleStyle == null) {
			titleStyle = this.createDefaultTitleStyle(workbook);
		}
		return titleStyle;
	}

//	public CellStyle createBodyStyle(Workbook workbook) {
//		if (bodyStyle == null) {
//			bodyStyle = this.createDefaultBodyStyle(workbook);
//			bodyStyleContext.put("default", bodyStyle);
//		}
//		return bodyStyle;
//	}
	
	public CellStyle createBodyStyle(Workbook workbook, String pattern) {
		String key = (pattern == null || pattern.length() < 1) ? "default" : pattern;
		CellStyle style = bodyStyleContext.get(key);
		if (style == null) {
			style = this.createDefaultBodyStyle(workbook);
			bodyStyleContext.put(key, style);
		}
		return style;
	}

	public Short getTitleBackgroundColor() {
		return titleBackgroundColor;
	}

	public void setTitleBackgroundColor(short titleBackgroundColor) {
		this.titleBackgroundColor = titleBackgroundColor;
	}

	public Short getBodyBackgroundColor() {
		return bodyBackgroundColor;
	}

	public void setBodyBackgroundColor(short bodyBackgroundColor) {
		this.bodyBackgroundColor = bodyBackgroundColor;
	}

	public Font getTitleFont() {
		return titleFont;
	}

	public void setTitleFont(Font titleFont) {
		this.titleFont = titleFont;
	}

	public Font getBodyFont() {
		return bodyFont;
	}

	public void setBodyFont(Font bodyFont) {
		this.bodyFont = bodyFont;
	}

	public void setTitleBackgroundColor(Short titleBackgroundColor) {
		this.titleBackgroundColor = titleBackgroundColor;
	}

	public void setBodyBackgroundColor(Short bodyBackgroundColor) {
		this.bodyBackgroundColor = bodyBackgroundColor;
	}
	
	private CellStyle createDefaultTitleStyle(Workbook workbook) {
		CellStyle style = null;
		style = workbook.createCellStyle();
//		style.setAlignment(CellStyle.ALIGN_CENTER); // 旧版
		style.setAlignment(HorizontalAlignment.CENTER);
//		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		if (titleBackgroundColor != null) {
//			style.setFillPattern(CellStyle.SOLID_FOREGROUND); // 旧版
			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			style.setFillForegroundColor(titleBackgroundColor);
		}
//		style.setBorderBottom(CellStyle.BORDER_THIN); // 旧版
//		style.setBorderTop(CellStyle.BORDER_THIN);
//		style.setBorderLeft(CellStyle.BORDER_THIN);
//		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		if (titleFont == null) {
			this.titleFont = createDefaultTitleFont(workbook);
		}
		style.setFont(titleFont);
		return style;
	}
	
	private CellStyle createDefaultBodyStyle(Workbook workbook) {
		CellStyle style = null;
		style = workbook.createCellStyle();
//		style.setAlignment(CellStyle.ALIGN_LEFT); // 旧版
//		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setAlignment(HorizontalAlignment.LEFT);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		if (this.bodyBackgroundColor != null) {
//			style.setFillPattern(CellStyle.SOLID_FOREGROUND); // 旧版
			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			style.setFillForegroundColor(bodyBackgroundColor);
		}
//		style.setBorderBottom(CellStyle.BORDER_THIN); // 旧版
//		style.setBorderTop(CellStyle.BORDER_THIN);
//		style.setBorderLeft(CellStyle.BORDER_THIN);
//		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		if (bodyFont == null) {
			this.bodyFont = createDefaultBodyFont(workbook);
		}
		style.setFont(bodyFont);
		return style;
	}

	/**
	 * 创建默认标题字体
	 */
	public static Font createDefaultTitleFont(Workbook workbook) {
//		return createFont(workbook, DEFAULT_BODY_FONTNAME, DEFAULT_BODY_FONTSIZE, Font.BOLDWEIGHT_BOLD); // 旧版
		return createFont(workbook, DEFAULT_BODY_FONTNAME, DEFAULT_BODY_FONTSIZE, true);
	}
	
	/**
	 * 创建默认正文字体
	 */
	public static Font createDefaultBodyFont(Workbook workbook) {
//		return createFont(workbook, DEFAULT_BODY_FONTNAME, DEFAULT_BODY_FONTSIZE, Font.BOLDWEIGHT_NORMAL); // 旧版
		return createFont(workbook, DEFAULT_BODY_FONTNAME, DEFAULT_BODY_FONTSIZE, false);
	}
	
	/**
	 * 创建字体
	 */
//	public static Font createFont(Workbook workbook, String fontName, short fontSize, short fontWeight) { // 旧版
	public static Font createFont(Workbook workbook, String fontName, short fontSize, boolean fontWeight) {
		Font font = workbook.createFont();
		font.setFontName(fontName);
		font.setFontHeightInPoints(fontSize);
//		font.setBoldweight(fontWeight);
		font.setBold(fontWeight);
		return font;
	}
	
}
