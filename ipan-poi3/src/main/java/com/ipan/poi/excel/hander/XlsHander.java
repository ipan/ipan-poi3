package com.ipan.poi.excel.hander;

import java.io.File;

import org.apache.poi.ss.usermodel.Row;

import com.ipan.poi.excel.config.XlsEntity;
import com.ipan.poi.excel.exception.XlsParserException;
import com.ipan.poi.excel.importer.XlsResult;


/**
 * XLS导入处理器接口
 * 抽取了对Excel的解析、验证、保存操作；
 * 
 * 自定义处理器，需要继承BaseXlsHander来实现；
 * 
 * @author iPan
 * @version 2013-09-15
 */
public interface XlsHander {
	/** 
	 * 初始化
	 * 在XlsImporter导入之前会被调用；
	 */
	public void init();
	
	/** 
	 * 解析单元格
	 * 子类可以继承BaseXlsHander并且覆盖setValue方法，默认会将常见的类型设置到实体内；
	 */
	public void parser(XlsEntity defEntity, Row row, Object formBean) throws XlsParserException;
	
	/** 
	 * 验证是否存在（true：存在；false：不存在）
	 * 子类可以继承BaseXlsHander并且覆盖doFind方法，返回实体类列表；默认会验证配置文件中指定valid="true"的字段；
	 */
	public boolean isExists(XlsEntity defEntity, Object formBean);
	
	/** 
	 * 保存记录
	 * 验证通过的话（记录不重复），则会插入；
	 * 验证不通过的话（记录重复），则会更新；
	 * 若ipan.poi.xlsImporter.validate=false，则只会插入；
	 */
	public void save(XlsEntity defEntity, Object formBean, XlsResult result) throws Exception;
	
	/** 
	 * 导入文件另存为
	 * 为了方便用户另存为，子类可以调用copyFile方法来复制文件；
	 */
	public void saveAsFile(File importFile);
	
}
