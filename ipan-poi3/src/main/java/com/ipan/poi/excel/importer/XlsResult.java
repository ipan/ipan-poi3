package com.ipan.poi.excel.importer;

import com.ipan.kits.base.Platforms;

/**
 * 导入结果Result；
 * 
 * @author iPan
 * @version 2013-09-15
 */
public class XlsResult {
	/** true：导入成功 false：导入失败 */
	private boolean status = false;
	
	/** 导入文件存放路径（成功：保存在成功目录下；失败：保存在失败目录下；） */
	private String importFile = null;
	
	/** 导入失败的记录文件路径（包含了具体没有被导入的记录） */
	private String failRecordFile = null;
	
	/** 导入结果文本 */
	private String text = null;
	
	/** 插入条数 */
	private int insertCount = 0;
	
	/** 更新条数 */
	private int updateCount = 0;
	
	/** 失败条数 */
	private int failCount = 0;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getImportFile() {
		return importFile;
	}

	public void setImportFile(String importFile) {
		this.importFile = importFile;
	}

	public String getFailRecordFile() {
		return failRecordFile;
	}

	public void setFailRecordFile(String failRecordFile) {
		this.failRecordFile = failRecordFile;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getInsertCount() {
		return insertCount;
	}
	
	public void addInsertCount() {
		this.insertCount++;
	}
	
	public void cleanInsertCount() {
		this.insertCount = 0;
	}

	public int getUpdateCount() {
		return updateCount;
	}
	
	public void addUpdateCount() {
		this.updateCount++;
	}
	
	public void cleanUpdateCount() {
		this.updateCount = 0;
	}

	public int getFailCount() {
		return failCount;
	}
	
	public void addFailCount() {
		this.failCount++;
	}
	
	public void cleanFailCount() {
		this.failCount = 0;
	}

	public void cleanCount() {
		this.failCount = 0;
		this.insertCount = 0;
		this.updateCount = 0;
	}
	
	public int getTotalCount() {
		return this.insertCount + this.updateCount + this.failCount;
	}
	
	public int getSuccessCount() {
		return this.insertCount + this.updateCount;
	}
	
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("status=").append(status).append(Platforms.LINE_SEPARATOR)
			.append("importFile=").append(importFile).append(Platforms.LINE_SEPARATOR)
			.append("failRecordFile=").append(failRecordFile).append(Platforms.LINE_SEPARATOR)
			.append("insertCount=").append(insertCount).append(Platforms.LINE_SEPARATOR)
			.append("updateCount=").append(updateCount).append(Platforms.LINE_SEPARATOR)
			.append("failCount=").append(failCount).append(Platforms.LINE_SEPARATOR)
			.append("text=").append(text).append(Platforms.LINE_SEPARATOR);
		return buf.toString();
	}
}
