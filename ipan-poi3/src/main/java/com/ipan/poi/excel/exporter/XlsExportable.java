package com.ipan.poi.excel.exporter;

import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.ipan.poi.excel.config.XlsEntity;
import com.ipan.poi.excel.service.XlsService;

/**
 * Excel导出接口；
 * 
 * XML配置 + 数据 = Excel输出
 * 
 * 2015-01-10 修正:
 * 单元格字体、样式需要在每次使用使用传入，只能供一次导出使用的时候重复使用；下次导出需要重新传入新的样式对象；
 * 所以，接口做了调整；
 * 
 * @author iPan
 * @version 2015-01-10
 */
public interface XlsExportable {

	/**
	 * 根据配置文件导出（静态查询）
	 * 查询条件写入配置文件中；
	 */
	public void exportByCfg(XlsEntity defEntity, Object entity, OutputStream out, CellStyleCreatable styleCreater);
	public void exportByCfg(XlsEntity defEntity, Object entity, OutputStream out, CellStyleCreatable style, int rowIndex, String sheetName);
	public void exportByCfg(XlsService<Object> service, XlsEntity defEntity, Object entity, OutputStream out, CellStyleCreatable style, 
			int rowIndex, String sheetName, Workbook workbook);
	
	/**
	 * 根据实体类列表导出（动态查询）
	 * 查询条件根据业务层的实现传递过来；
	 */
	public void exportByEntity(XlsEntity defEntity, List<?> entityList, OutputStream out, CellStyleCreatable styleCreater);
	public void exportByEntity(XlsEntity defEntity, List<?> entityList, OutputStream out, CellStyleCreatable style, 
			int rowIndex, String sheetName, Workbook workbook);
	
	/**
	 * 根据Map列表导出（动态查询）
	 * 查询条件根据业务层的实现传递过来；
	 */
	public void exportByMap(XlsEntity defEntity, List<Map<String, Object>> list, OutputStream out, CellStyleCreatable styleCreater);
	public void exportByMap(XlsEntity defEntity, List<Map<String, Object>> list, OutputStream out, CellStyleCreatable style, 
			int rowIndex, String sheetName, Workbook workbook);
	
	/**
	 * 根据JDBC返回结果集导出（动态查询）
	 */
	public void exportByResultSet(XlsEntity defEntity, ResultSet resultSet, OutputStream out, CellStyleCreatable styleCreater);
	public void exportByResultSet(XlsEntity defEntity, ResultSet resultSet, OutputStream out, CellStyleCreatable style, 
			int rowIndex, String sheetName, Workbook workbook);
	
	/**
	 * 获取导出目录；
	 */
	public String getExportDir();

}