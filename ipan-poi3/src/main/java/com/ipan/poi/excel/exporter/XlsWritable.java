package com.ipan.poi.excel.exporter;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;

import com.ipan.poi.excel.config.XlsEntity;


/**
 * 写入操作接口；
 * 
 * @author iPan
 * @version 2013-09-20
 */
public interface XlsWritable {
	
	/**
	 * 根据实体类写入至Sheet（接口一）；
	 */
	public int writeByEntity(XlsEntity defEntity, Sheet sheet, CellStyleCreatable style, int rowIndex, List<?> entityList);
	
	/**
	 * 根据数组写入至Sheet（接口二）；
	 */
	public int writeByMap(XlsEntity defEntity, Sheet sheet, CellStyleCreatable style, int rowIndex, List<Map<String, Object>> list);
	
	/**
	 * 根据JDBC结果集写入至Sheet（接口三）；
	 */
	public int writeByResultSet(XlsEntity defEntity, Sheet sheet, CellStyleCreatable style, int rowIndex, ResultSet resultSet);
	
}
