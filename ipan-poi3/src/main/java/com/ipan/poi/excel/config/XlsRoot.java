package com.ipan.poi.excel.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.ipan.kits.base.Platforms;

/**
 * 根元素配置；
 * 
 * @author iPan
 * @version 2013-09-14
 */
public abstract class XlsRoot implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -8312751658376091125L;
	private Map<String, XlsEntity> entities = new TreeMap<String, XlsEntity>();
	private String root = "";
	
	public XlsRoot(String root) {
		this.root = root;
	}
	
	public List<XlsEntity> getEntities() {
		return new ArrayList<XlsEntity>(entities.values());
	}

	public void clearEntity() {
		entities.clear();
	}
	
	public XlsRoot addEntity(XlsEntity entity) {
		entities.put(entity.getClassName(), entity);
		return this;
	}
	
	public void removeEntity(String key) {
		if (entities.size() > 0) {
			entities.remove(key);
		}
	}
	
	public XlsEntity getEntity(String className) {
		return entities.get(className);
	}
	
	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>").append(Platforms.LINE_SEPARATOR);
		buf.append("<").append(root).append(">").append(Platforms.LINE_SEPARATOR);
		for (XlsEntity entity : entities.values()) {
			buf.append(entity.toString());
		}
		buf.append("</").append(root).append(">").append(Platforms.LINE_SEPARATOR);
		return buf.toString();
	}
}
