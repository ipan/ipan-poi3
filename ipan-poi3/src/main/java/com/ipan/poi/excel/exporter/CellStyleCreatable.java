package com.ipan.poi.excel.exporter;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * 格式化接口；
 * 
 * @author iPan
 * @version 2013-09-15
 */
public interface CellStyleCreatable {

	/**
	 * 返回标题格式化样式；
	 */
	public CellStyle createTitleStyle(Workbook workbook);
	
	/**
	 * 返回正文格式化样式；
	 */
//	@Deprecated
//	public CellStyle createBodyStyle(Workbook workbook);
	
	/**
	 * 返回正文格式化样式；
	 */
	public CellStyle createBodyStyle(Workbook workbook, String pattern); // 2015-01-10修正
	
}
