package com.ipan.poi.excel.importer;

import java.io.File;

import com.ipan.poi.excel.hander.XlsHander;
/**
 * XLS导入功能定义；
 * 
 * @author iPan
 * @version 2013-09-15
 */
public interface XlsImportable {
	//-- 导入接口 --//
	/**
	 * 导入一个sheet，使用默认解析器；
	 */
	public XlsResult importExcel(File file, String fileName, String className);
	/**
	 * 指定解析器，导入一个sheet；
	 */
	public XlsResult importExcel(File file, String fileName, String className, XlsHander hander);
	/**
	 * 导入多个sheet，使用默认解析器；
	 */
	public XlsResult importExcel(File file, String fileName, String className, boolean mlutiSheet);
	/**
	 * 指定解析器，导入多个sheet；
	 */
	public XlsResult importExcel(File file, String fileName, String className, boolean mlutiSheet, XlsHander hander);
	
	//-- 获取目录路径 --//
	/**
	 * 获取导入目录；
	 */
	public String getImportDir();
	/**
	 * 获取导入成功目录；
	 */
	public String getImportSuccessDir();
	/**
	 * 获取导入失败目录；
	 */
	public String getImportFailDir();
	
	//-- 设置/获取导入Sheet中的内容与标题的位置 --//
	/**
	 * 获取标题位置，从0开始；
	 */
	public int getTitleRowIndex();
	/**
	 * 设置标题的位置；
	 */
	public void setTitleRowIndex(int titleRowIndex);
	/**
	 * 获取内容位置，从0开始；
	 */
	public int getBodyRowIndex();
	/**
	 * 设置内容的位置；
	 */
	public void setBodyRowIndex(int bodyRowIndex);
}
