package com.ipan.poi.excel.config;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.ipan.kits.base.Platforms;

/**
 * 实体字段格式配置
 * 
 * 注意：enable、valid只在导入时候使用，导出时候配置无效！
 * 
 * @author iPan
 * @version 2013-09-14
 */
public class XlsProperty implements Cloneable, Comparable<XlsProperty>, Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7200385228648573544L;
	// 常量
	public static final String ELE_PROPERTY = "xlsProperty";
	public static final String PRO_NAME = "name";
	public static final String PRO_TITLE = "title";
	public static final String PRO_ENABLE = "enable";
	public static final String PRO_PATTERN = "pattern";
	public static final String PRO_VALID = "valid";
	public static final String PRO_SEARCH = "search";
	/** 字段名称 */
	private String name = "";
	/** XLS文件标题 */
	private String title = "";
	/** 是否有效 true：有效 false：无效 */
	private boolean enable = true; // 导入使用
	/** 字段格式（日期格式可以指定字符串格式） */
	private String pattern = ""; // 导入导出使用
	/** 是否验证（在做导入的时候，使用标识为true的字段先验证记录是否存在；） */
	private boolean valid = false; // 导入使用
	/** 查询条件设置（在导出的时候，可以配置查询条件；） */
	private String search = ""; // 独立导出使用
	/** 实体元素 */
	private XlsEntity entity;

	public XlsProperty(XlsEntity entity, String name, String title) {
		this.entity = entity;
		this.name = name;
		this.title = title;
	}
	
	public XlsProperty(XlsEntity entity, String name, String title, boolean enable) {
		this.entity = entity;
		this.name = name;
		this.title = title;
		this.enable = enable;
	}

	public XlsProperty(XlsEntity entity, String name, String title, boolean enable, String pattern) {
		this.entity = entity;
		this.name = name;
		this.title = title;
		this.enable = enable;
		this.pattern = pattern;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	
	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public XlsEntity getEntity() {
		return entity;
	}

	public void setEntity(XlsEntity entity) {
		this.entity = entity;
	}

	@Override
	public XlsProperty clone() throws CloneNotSupportedException {
		return (XlsProperty) super.clone();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("<").append(ELE_PROPERTY).append(" ");
		if (StringUtils.isNotEmpty(this.name)) {
			buf.append(PRO_NAME).append("=\"").append(this.name).append("\" ");
		}
		if (StringUtils.isNotEmpty(this.title)) {
			buf.append(PRO_TITLE).append("=\"").append(this.title).append("\" ");
		}
		buf.append(PRO_ENABLE).append("=\"").append(this.enable).append("\" ");
		if (StringUtils.isNotEmpty(this.pattern)) {
			buf.append(PRO_PATTERN).append("=\"").append(this.pattern).append("\" ");
		}
		// 验证字段为true才显示
		if (this.valid) {
			buf.append(PRO_VALID).append("=\"").append(this.valid).append("\" ");
		}
		if (StringUtils.isNotEmpty(this.search)) {
			buf.append(PRO_SEARCH).append("=\"").append(this.search).append("\" ");
		}
		buf.append("/>").append(Platforms.LINE_SEPARATOR);
		return buf.toString();
	}

	public int compareTo(XlsProperty o) {
		if (o == null) {
			return 1;
		}
		if (this.name == null) {
			return -1;
		}
		return this.name.compareTo(o.getName());
	}

}
