package com.ipan.poi.excel.config;


/**
 * 导入配置文件信息
 * 
 * @author iPan
 * @version 2013-9-14
 */
public class ImportConfiguration extends XlsConfiguration {

	public final static String DEFAULT_PATH = "xls_import_cfg.xml";
	
	private static ImportConfiguration INSTANCE = null;
	
	private String filePath = DEFAULT_PATH;
	
	private ImportConfiguration() {
		super(new XlsImports());
		flushConfig();
	}
	
	private ImportConfiguration(String filePath) {
		super(new XlsImports());
		this.filePath = filePath;
		flushConfig();
	}
	
	@Override
	public void flushConfig() {
		initDefaultConfig(this.filePath);
	}
	
	public synchronized static ImportConfiguration getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ImportConfiguration();
		}
		return INSTANCE;
	}
	
	public synchronized static ImportConfiguration getInstance(String filePath) {
		if (INSTANCE == null) {
			INSTANCE = new ImportConfiguration(filePath);
		}
		return INSTANCE;
	}
	
}
