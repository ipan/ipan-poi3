package com.ipan.poi.excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ipan.kits.io.FileUtil;

/**
 * 工作簿工具类
 * 
 * @author iPan
 * @version 2013-9-15
 */
public class WorkbookFactory extends org.apache.poi.ss.usermodel.WorkbookFactory {

	/**
	 *  根据文件扩展名创建Workbook
	 */
	public static Workbook createByFileExtension(String filePath) { // <=65536条
		if (filePath == null || filePath.length() < 1) {
			return null;
		}
		Workbook workbook = null;
		String extension = FileUtil.getFileExtension(filePath);
		if ("xls".equals(extension)) {
			workbook = createHSSFWorkbook();
		} else if ("xlsx".equals(extension)) {
			workbook = createXSSFWorkbook();
		} else {
			throw new IllegalArgumentException("非法的文件名！");
		}
		return workbook;
	}
	
	public static Workbook createSXSSFWorkbookByFileExtension(String filePath) { // 超过65536条使用这个
		if (filePath == null || filePath.length() < 1) {
			return null;
		}
		String extension = FileUtil.getFileExtension(filePath);
		if (!"xlsx".equals(extension)) {
			throw new IllegalArgumentException("非法的文件名！文件扩展名必须是.xlsx！");
		}
		return createSXSSFWorkbook();
	}
	
	public static Workbook createHSSFWorkbook() {
		return new HSSFWorkbook();
	}
	
	public static Workbook createXSSFWorkbook() {
		return new XSSFWorkbook();
	}
	
	public static Workbook createSXSSFWorkbook() {
		return new SXSSFWorkbook();
	}
}
