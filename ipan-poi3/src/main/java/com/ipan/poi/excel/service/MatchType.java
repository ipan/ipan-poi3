package com.ipan.poi.excel.service;

/**
 * 属性比较类型
 * 
 * @author iPan
 * @version 2014-1-16
 */
public enum MatchType {
	
	/**
	 * EQ: 等于；
	 * LIKE: 模糊匹配；
	 * LT: 小于；
	 * GT: 大于；
	 * LE: 小于等于；
	 * GE: 大于等于；
	 */
	EQ("="), LIKE("like"), LT("<"), GT(">"), LE("<="), GE(">=");
	
	/** 标签 */
	private String tag = "";
	
	private MatchType(String tag) {
		this.tag = tag;
	}
	
	public String getTag() {
		return tag;
	}
	
}
