package com.ipan.poi.easyexcel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * EasyExcel支持的Java类型
 * 
 * 注意：基础类型int、byte、long、double、float、short、boolean使用包装类型；
 * 
 * @author iPan
 * @date 2022-01-24
 */
public final class JavaTypeSupport {
	// 字符串
	public static final Class<String> STRING = String.class;
	// 整型
	public static final Class<Byte> BYTE = Byte.class;
	public static final Class<Short> SHORT = Short.class;
	public static final Class<Integer> INTEGER = Integer.class;
	public static final Class<Long> LONG = Long.class;
	// 浮点型
	public static final Class<Float> FLOAT = Float.class;
	public static final Class<Double> DOUBLE = Double.class;
	// 金额
	public static final Class<BigDecimal> BIGDECIMAL = BigDecimal.class;
	// 布尔
	public static final Class<Boolean> BOOLEAN = Boolean.class;
	// 日期时间
	public static final Class<Date> DATE = Date.class;
	
	/**
	 * 获取支持的Java类型列表
	 * 前端显示可以调用getSimpleName()获取简短名称
	 */
	public static List<Class> getSupportType() {
		List<Class> list = new ArrayList<>();
		list.add(STRING);
		list.add(BYTE);
		list.add(SHORT);
		list.add(INTEGER);
		list.add(LONG);
		list.add(FLOAT);
		list.add(DOUBLE);
		list.add(BIGDECIMAL);
		list.add(BOOLEAN);
		list.add(DATE);
		return list;
	}
	
}
