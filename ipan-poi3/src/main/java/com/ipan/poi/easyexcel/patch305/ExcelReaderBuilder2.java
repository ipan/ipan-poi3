package com.ipan.poi.easyexcel.patch305;

import java.io.File;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;

import com.alibaba.excel.cache.ReadCache;
import com.alibaba.excel.cache.selector.ReadCacheSelector;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.enums.CellExtraTypeEnum;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.event.SyncReadListener;
import com.alibaba.excel.read.builder.AbstractExcelReaderParameterBuilder;
import com.alibaba.excel.read.listener.ModelBuildEventListener;
import com.alibaba.excel.read.metadata.ReadWorkbook;
import com.alibaba.excel.support.ExcelTypeEnum;

/**
 * ExcelReaderBuilder2补丁
 * 
 * @author iPan
 * @date 2022-01-30
 */
public class ExcelReaderBuilder2 extends AbstractExcelReaderParameterBuilder<ExcelReaderBuilder2, ReadWorkbook> {
    /**
     * Workbook
     */
    private ReadWorkbook readWorkbook;

    public ExcelReaderBuilder2() {
        this.readWorkbook = new ReadWorkbook();
    }

    public ExcelReaderBuilder2 excelType(ExcelTypeEnum excelType) {
        readWorkbook.setExcelType(excelType);
        return this;
    }

    /**
     * Read InputStream
     * <p>
     * If 'inputStream' and 'file' all not empty, file first
     */
    public ExcelReaderBuilder2 file(InputStream inputStream) {
        readWorkbook.setInputStream(inputStream);
        return this;
    }

    /**
     * Read file
     * <p>
     * If 'inputStream' and 'file' all not empty, file first
     */
    public ExcelReaderBuilder2 file(File file) {
        readWorkbook.setFile(file);
        return this;
    }

    /**
     * Read file
     * <p>
     * If 'inputStream' and 'file' all not empty, file first
     */
    public ExcelReaderBuilder2 file(String pathName) {
        return file(new File(pathName));
    }

    /**
     * Mandatory use 'inputStream' .Default is false.
     * <p>
     * if false, Will transfer 'inputStream' to temporary files to improve efficiency
     */
    public ExcelReaderBuilder2 mandatoryUseInputStream(Boolean mandatoryUseInputStream) {
        readWorkbook.setMandatoryUseInputStream(mandatoryUseInputStream);
        return this;
    }

    /**
     * Default true
     *
     * @param autoCloseStream
     * @return
     */
    public ExcelReaderBuilder2 autoCloseStream(Boolean autoCloseStream) {
        readWorkbook.setAutoCloseStream(autoCloseStream);
        return this;
    }

    /**
     * Ignore empty rows.Default is true.
     *
     * @param ignoreEmptyRow
     * @return
     */
    public ExcelReaderBuilder2 ignoreEmptyRow(Boolean ignoreEmptyRow) {
        readWorkbook.setIgnoreEmptyRow(ignoreEmptyRow);
        return this;
    }

    /**
     * This object can be read in the Listener {@link AnalysisEventListener#invoke(Object, AnalysisContext)}
     * {@link AnalysisContext#getCustom()}
     *
     * @param customObject
     * @return
     */
    public ExcelReaderBuilder2 customObject(Object customObject) {
        readWorkbook.setCustomObject(customObject);
        return this;
    }

    /**
     * A cache that stores temp data to save memory.
     *
     * @param readCache
     * @return
     */
    public ExcelReaderBuilder2 readCache(ReadCache readCache) {
        readWorkbook.setReadCache(readCache);
        return this;
    }

    /**
     * Select the cache.Default use {@link com.alibaba.excel.cache.selector.SimpleReadCacheSelector}
     *
     * @param readCacheSelector
     * @return
     */
    public ExcelReaderBuilder2 readCacheSelector(ReadCacheSelector readCacheSelector) {
        readWorkbook.setReadCacheSelector(readCacheSelector);
        return this;
    }

    /**
     * Whether the encryption
     *
     * @param password
     * @return
     */
    public ExcelReaderBuilder2 password(String password) {
        readWorkbook.setPassword(password);
        return this;
    }

    /**
     * SAXParserFactory used when reading xlsx.
     * <p>
     * The default will automatically find.
     * <p>
     * Please pass in the name of a class ,like : "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl"
     *
     * @see SAXParserFactory#newInstance()
     * @see SAXParserFactory#newInstance(String, ClassLoader)
     * @param xlsxSAXParserFactoryName
     * @return
     */
    public ExcelReaderBuilder2 xlsxSAXParserFactoryName(String xlsxSAXParserFactoryName) {
        readWorkbook.setXlsxSAXParserFactoryName(xlsxSAXParserFactoryName);
        return this;
    }

    /**
     * Read some extra information, not by default
     *
     * @param extraType
     *            extra information type
     * @return
     */
    public ExcelReaderBuilder2 extraRead(CellExtraTypeEnum extraType) {
        if (readWorkbook.getExtraReadSet() == null) {
            readWorkbook.setExtraReadSet(new HashSet<CellExtraTypeEnum>());
        }
        readWorkbook.getExtraReadSet().add(extraType);
        return this;
    }

    /**
     * Whether to use the default listener, which is used by default.
     * <p>
     * The {@link ModelBuildEventListener} is loaded by default to convert the object.
     *
     * @param useDefaultListener
     * @return
     */
    public ExcelReaderBuilder2 useDefaultListener(Boolean useDefaultListener) {
        readWorkbook.setUseDefaultListener(useDefaultListener);
        return this;
    }

    public ExcelReader2 build() {
        return new ExcelReader2(readWorkbook);
    }

    public void doReadAll() {
        ExcelReader2 excelReader = build();
        excelReader.readAll();
        excelReader.finish();
    }

    /**
     * Synchronous reads return results
     *
     * @return
     */
    public <T> List<T> doReadAllSync() {
        SyncReadListener syncReadListener = new SyncReadListener();
        registerReadListener(syncReadListener);
        ExcelReader2 excelReader = build();
        excelReader.readAll();
        excelReader.finish();
        return (List<T>)syncReadListener.getList();
    }

    public ExcelReaderSheetBuilder2 sheet() {
        return sheet(null, null);
    }

    public ExcelReaderSheetBuilder2 sheet(Integer sheetNo) {
        return sheet(sheetNo, null);
    }

    public ExcelReaderSheetBuilder2 sheet(String sheetName) {
        return sheet(null, sheetName);
    }

    public ExcelReaderSheetBuilder2 sheet(Integer sheetNo, String sheetName) {
        ExcelReaderSheetBuilder2 excelReaderSheetBuilder = new ExcelReaderSheetBuilder2(build());
        if (sheetNo != null) {
            excelReaderSheetBuilder.sheetNo(sheetNo);
        }
        if (sheetName != null) {
            excelReaderSheetBuilder.sheetName(sheetName);
        }
        return excelReaderSheetBuilder;
    }

    @Override
    protected ReadWorkbook parameter() {
        return readWorkbook;
    }
}