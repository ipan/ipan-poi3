package com.ipan.poi.easyexcel.listener;

import java.util.Map;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

/**
 * 同步读取excel标题信息
 * 
 * @author iPan
 * @date 2022-01-25
 */
public class SyncReadHeadListener extends AnalysisEventListener<Object> {
	
	private Map<Integer, String> headMap = null;
	private Integer headRowNumber = null;
	private Integer rowIndex = null;

	@Override
	public void invoke(Object data, AnalysisContext context) {
	}

	@Override
	public void doAfterAllAnalysed(AnalysisContext context) {
	}
	
	@Override
	public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
		headRowNumber = context.readSheetHolder().getHeadRowNumber();
		rowIndex = context.readSheetHolder().getRowIndex();
		this.headMap = headMap;
	}

	@Override
	public boolean hasNext(AnalysisContext context) {
		if (headRowNumber != null && rowIndex != null && rowIndex + 1 >= headRowNumber) {
			return false;
		}
		return true;
	}

	/**
	 * 获取头部标题
	 */
	public Map<Integer, String> getHeadMap() {
		return headMap;
	}
	
}
