package com.ipan.poi.easyexcel.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

/**
 * 同步读取excel前N行数据
 * 
 * @author iPan
 * @date 2022-01-25
 */
public class SyncReadListener2 extends AnalysisEventListener<Map<Integer, String>> {
	/** 
	 * 是否跳过空行
	 * 默认EasyExcel跳过的空行是指没有任何样式的空行，一旦加了边线的空行是识别不出来的；
	 */
	private boolean ignoreEmptyRow = true;
	private List<Map<Integer, String>> list = new ArrayList<Map<Integer, String>>();

	public SyncReadListener2() {}
	
    public SyncReadListener2(boolean ignoreEmptyRow) {
		this.ignoreEmptyRow = ignoreEmptyRow;
	}

	public boolean isIgnoreEmptyRow() {
		return ignoreEmptyRow;
	}

	public void setIgnoreEmptyRow(boolean ignoreEmptyRow) {
		this.ignoreEmptyRow = ignoreEmptyRow;
	}

	@Override
    public void invoke(Map<Integer, String> object, AnalysisContext context) {
    	String values = StringUtils.join(object.values(), "").trim();
    	if (ignoreEmptyRow && StringUtils.isBlank(values)) { // 跳过空行
//    		System.out.println("ignore empty row");
    		return ;
    	}
        list.add(object);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {}

    public List<Map<Integer, String>> getList() {
        return list;
    }

    public void setList(List<Map<Integer, String>> list) {
        this.list = list;
    }
    
}
