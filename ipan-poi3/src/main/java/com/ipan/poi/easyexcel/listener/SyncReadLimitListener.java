package com.ipan.poi.easyexcel.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;

/**
 * 同步读取excel前N行数据
 * 
 * @author iPan
 * @date 2022-01-25
 */
public class SyncReadLimitListener implements ReadListener<Map<Integer, String>> {
	
	private List<Map<Integer, String>> data = null;
	private Integer n = null;
	private int index = 0;
	
	public SyncReadLimitListener(Integer n) {
		this.n = n;
		this.data = new ArrayList<>(n);
	}

	@Override
	public void invoke(Map<Integer, String> rowMap, AnalysisContext context) {
		this.data.add(rowMap);
		index++;
	}

	@Override
	public void doAfterAllAnalysed(AnalysisContext context) {
	}

	@Override
	public boolean hasNext(AnalysisContext context) {
		return index >= n ? false : true;
	}
	
	public List<Map<Integer, String>> getData() {
		return this.data;
	}
}
