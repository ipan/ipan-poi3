package com.ipan.poi.easyexcel.patch305;

import java.io.IOException;

import org.apache.commons.csv.CSVParser;

import com.alibaba.excel.context.AnalysisContextImpl;
import com.alibaba.excel.context.csv.CsvReadContext;
import com.alibaba.excel.read.metadata.ReadWorkbook;
import com.alibaba.excel.read.metadata.holder.csv.CsvReadSheetHolder;
import com.alibaba.excel.read.metadata.holder.csv.CsvReadWorkbookHolder;
import com.alibaba.excel.support.ExcelTypeEnum;

/**
 * DefaultCsvReadContext补丁
 * 
 * @author iPan
 * @date 2022-01-30
 */
public class DefaultCsvReadContext2 extends AnalysisContextImpl implements CsvReadContext {
	
	private CSVParser csvParser = null;

    public DefaultCsvReadContext2(ReadWorkbook readWorkbook, ExcelTypeEnum actualExcelType) {
        super(readWorkbook, actualExcelType);
    }
    
    public CSVParser getCsvParser() {
		return csvParser;
	}

	public void setCsvParser(CSVParser csvParser) {
		this.csvParser = csvParser;
	}
	
	public void closeCsvParser() {
		if (csvParser != null) {
			try {
				csvParser.close();
			} catch (IOException e) {
			}
		}
	}

    @Override
    public CsvReadWorkbookHolder csvReadWorkbookHolder() {
        return (CsvReadWorkbookHolder)readWorkbookHolder();
    }

    @Override
    public CsvReadSheetHolder csvReadSheetHolder() {
        return (CsvReadSheetHolder)readSheetHolder();
    }

}
