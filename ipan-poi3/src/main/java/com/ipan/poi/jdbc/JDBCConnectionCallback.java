package com.ipan.poi.jdbc;

import java.sql.Connection;

/**
 * Connection回调
 * 
 * @author iPan
 * @version 2013-8-7
 */
public interface JDBCConnectionCallback {

	public void doInConnection(Connection conn);
}
