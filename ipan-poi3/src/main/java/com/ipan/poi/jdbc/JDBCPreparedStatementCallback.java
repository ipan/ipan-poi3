package com.ipan.poi.jdbc;

import java.sql.PreparedStatement;

/**
 * PreparedStatement回调
 * 
 * @author iPan
 * @version 2013-8-7
 */
public interface JDBCPreparedStatementCallback {

	public void doInPreparedStatement(PreparedStatement ps);
}
