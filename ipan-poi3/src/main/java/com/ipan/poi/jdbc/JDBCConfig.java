package com.ipan.poi.jdbc;

/**
 * JDBC配置信息
 * 
 * @author iPan
 * @version 2013-8-7
 */
public class JDBCConfig {

	private String driver = null;
	private String url = null;
	private String userName = null;
	private String passWord = null;
	private String catalog = null;
	private String schema = null;

	public JDBCConfig(String driver, String url, String userName, String passWord) {
		this(driver, url, userName, passWord, null, null);
	}
	
	public JDBCConfig(String driver, String url, String userName, String passWord, String catalog, String schema) {
		this.driver = driver;
		this.url = url;
		this.userName = userName;
		this.passWord = passWord;
		this.catalog = catalog;
		this.schema = schema;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

}
