package com.ipan.poi.jdbc;

import java.sql.Statement;

/**
 * Statement回调
 * 
 * @author iPan
 * @version 2013-8-7
 */
public interface JDBCStatementCallback {

	public void doInStatement(Statement stmt);
}
